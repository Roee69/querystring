$(function () {
    const $addBtn = $('#addBtn'),
        $btnSubmit = $('#btnSubmit');
    let btnIndex = 0;
    $addBtn.on('click', () => {
        if ($("#queryEnter").is(":hidden")) {
            $('#insertDiv').show();
        } else if ($("#error").length == 0) {
            $('<span id="error">Please save a query in order to add a new one</span>').insertAfter($('#insertDiv'));
        }
    });
    $btnSubmit.on('click', () => {
        let $queryName = $('#queryName'),
        $queryEnter = $('#queryEnter');
        localStorage.setItem('btn' + btnIndex, $('#queryName').val());
        localStorage.setItem('query' + btnIndex, $('#queryEnter').val());
        let btnName = localStorage.getItem('btn' + btnIndex);
        let btnQuery = localStorage.getItem('query' + btnIndex);
        $('<button class="button queryBtn" id="btn'+btnIndex+'" query-data="'+btnQuery+'">'+btnName+'</button>').insertBefore($('.input-group'))
        $('#insertDiv').hide();
        $queryName.val('');
        $queryEnter.val('');
    });
    $('body').on('click', 'button.queryBtn', (e)=> {
        let urlQuery = $(e.currentTarget).attr('query-data');
        console.log('btn was clicked, now '+ urlQuery +' will be added to the url');
    });
});

// // by passing an object you can define default values e.g.: []
// chrome.storage.local.get({userKeyIds: []}, function (result) {
//     // the input argument is ALWAYS an object containing the queried keys
//     // so we select the key we need
//     var userKeyIds = result.userKeyIds;
//     userKeyIds.push({keyPairId: keyPairId, HasBeenUploadedYet: false});
//     // set the new array value to the same key
//     chrome.storage.local.set({userKeyIds: userKeyIds}, function () {
//         // you can use strings instead of objects
//         // if you don't  want to define default values
//         chrome.storage.local.get('userKeyIds', function (result) {
//             console.log(result.userKeyIds)
//         });
//     });
// });